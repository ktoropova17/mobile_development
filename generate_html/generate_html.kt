import java.io.File

fun main() {
    data class Message(val address: String?, val topic: String?, val sender: String?, val message: String?) {
        fun toHTML(): String {
            val template = StringBuilder()
            template.append("<div style='padding: 15px; border: 1px solid #000000; box-shadow: rgba(50, 50, 93, 0.25) 0px 6px 12px -2px, rgba(0, 0, 0, 0.3) 0px 3px 7px -3px; width: 400px;'>\n")
            template.append("<h2 style='background-color: #ffc0cb; color: #ffffff; padding: 10px; text-align: center;'>Данные о сообщении</h2>\n")
            template.append("<table style='font-family: Arial; width: 100%; background-color: #dcdcdc;'>\n")
            address?.let { template.append("<tr><td style='padding: 10px;'>Электронный адрес:</td><td style='padding: 10px;'>$it</td></tr>\n") }
            topic?.let { template.append("<tr><td style='padding: 10px;'>Тема:</td><td style='padding: 10px;'>$it</td></tr>\n") }
            sender?.let { template.append("<tr><td style='padding: 10px;'>Отправитель:</td><td style='padding: 10px;'>$it</td></tr>\n") }
            message?.let { template.append("<tr><td style='padding: 10px;'>Сообщение:</td><td style='padding: 10px;'>$it</td></tr>\n") }
            template.append("</table>\n")
            template.append("</div>\n")
            return template.toString()
        }
    }

    val m = Message("morgenshtern@gmail.com", null, "shaman", "ya russkiy")
    val htmlContent = m.toHTML()

    File("index.html").writeText(htmlContent)
}