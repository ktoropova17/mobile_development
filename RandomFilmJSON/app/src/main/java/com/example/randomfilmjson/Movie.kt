package com.example.randomfilmjson

data class Movie(val rating: String, val title: String, val genre: List<String>, val duration: String, val country: List<String>,
                 val year: String)